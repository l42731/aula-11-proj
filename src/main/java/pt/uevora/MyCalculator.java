package pt.uevora;

import java.util.Scanner;
import java.lang.Math;

public class MyCalculator {

    public Double execute(String expression){
        String[] split = expression.split("\\+");
        String[] split1 = expression.split("\\-");
        String[] split2 = expression.split("\\*");
        String[] split3 = expression.split("\\/");
        String[] split4 = expression.split("\\^");

        if(expression.contains("+")){
        	return sum(split[0], split[1]);
        
    	}else if(expression.contains("-")) {
    		return sub(split1[0], split1[1]);
    	
    	}else if(expression.contains("*")) {
    		return mult(split2[0], split2[1]);
    	
    	}else if(expression.contains("/")) {
    		return div(split3[0], split3[1]);
    		
    	}else if(expression.contains("^")) {
    		return potencia(split4[0], split4[1]);
    		
    	}else {
    		throw new IllegalArgumentException("Invalid expression!");
    	}
    }

    private Double sum(String arg1, String arg2) {
        return new Double(arg1) + new Double(arg2);
    }
    
    private Double sub(String arg1, String arg2) {
        return Double.valueOf(arg1) - Double.valueOf(arg2);
    }
    
    private Double mult(String arg1, String arg2) {
        return Double.valueOf(arg1) * Double.valueOf(arg2);
    }
    
    private Double div(String arg1, String arg2) {
    	return Double.valueOf(arg1) / Double.valueOf(arg2);
   }
    
    private Double potencia(String arg1, String arg2) {
       return Math.pow(Double.valueOf(arg1), Double.valueOf(arg2) );
    }

    public static void main(String[] args) {
        System.out.println("Calculator");
        System.out.println("Enter your expression:");
        Scanner scanner = new Scanner(System.in);
        String expression = scanner.nextLine();

        MyCalculator myCalculator = new MyCalculator();
        Object result = myCalculator.execute(expression);

        System.out.println("Result :  " + result);
    }

}
