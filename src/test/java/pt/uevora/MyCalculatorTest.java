package pt.uevora;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class MyCalculatorTest {

    MyCalculator calculator;

    @Before
    public void setUp(){
        calculator = new MyCalculator();
    }

    @After
    public void down(){
        calculator = null;
    }

    @Test
    public void testSum() throws Exception {
        Double result = calculator.execute("2+3");

        assertEquals("The sum result of 2 + 3 must be 5",  5D, (Object)result);
    }
    
    @Test
    public void testSub() throws Exception {
        
        Double result = calculator.execute("5-1");
        		
        assertEquals("The subtract result of 5 - 1 must be 4", 4D, (Object)result);
    }
    
    @Test
    public void testMult() throws Exception {
        
        Double result = calculator.execute("5*3");
        		
        assertEquals("The multiplication result of 5 x 3 must be 15", 15D, (Object)result);
    }
    
    @Test
    public void testDiv() throws Exception {
        
        Double result = calculator.execute("15/5");
        		
        assertEquals("The division result of 15 / 5 must be 3", 3D, (Object)result);
    }
    
    @Test
    public void testDivByZero() throws Exception {
        
        Double result = calculator.execute("15/0");
        		
        assertEquals("The division result of 15 / 0 must be infinity", true, Double.isInfinite(result));
    }
    
    @Test
    public void testPotencia() throws Exception {
        
        Double result = calculator.execute("3^4");
        		
        assertEquals("The result of 3^4 must be 81", 81D, (Object)result);   
    }
    
   // @Test
   // public void testException() throws Exception {
   //     assertThrows(java.lang.NumberFormatException.class, () -> {
   //     	calculator.execute("w+3");
   //     });
   // }
}